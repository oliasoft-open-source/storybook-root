#!/bin/sh

# This script sets up kubectl on an alpine build container in GitLab
# The following environment-dependent variables must be set properly before running:
# - $KUBE_NAME
#     - Name on the Kubernetes cluster
#     - Ex: OliasoftAKSCluster-test
# - $KUBE_URL
#     - URL for the cluster
#     - Ex: https://oliasoftakscluster-test-a2286d09.hcp.westeurope.azmk8s.io:443
# - $GITLAB_CD_SERVICEACCOUNT_NAME
#     - Service account name in Kubernetes cluster, to be used for authenticating by GitLab
#     - Can be found in k8s by running `kubectl get serviceaccounts` (should list one per application/service)
#     - Maintained/created from script in repo `infrastructure`, under `gitlab/gitlab-serviceaccount-rolebindings.yaml`
#     - Usually set in GitLab CI/CD Variables, here https://gitlab.com/oliasoft/wellplan/-/settings/ci_cd
#     - Ex GITLAB_CD_SERVICEACCOUNT_NAME=$GITLAB_CD_SERVICEACCOUNT_NAME_TEST
# - $GITLAB_CD_SERVICEACCOUNT_TOKEN
#     - Same as for corresponding GITLAB_CD_SERVICEACCOUNT_NAME-variable.
#     - Can be found in k8s by running (replace <app-name> by e.g. 'welldesign'):
#         `GITLAB_SECRET_NAME=$(kubectl get serviceaccount gitlab-cd-<app-name> -o jsonpath='{.secrets[0].name}')`
#         `kubectl get secret $GITLAB_SECRET_NAME -o jsonpath='{.data.token}' | base64 -d`

apk add --no-cache curl
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
mv ./kubectl /usr/local/bin/kubectl
kubectl config set-cluster $KUBE_NAME --server="$KUBE_URL" --insecure-skip-tls-verify=true
kubectl config set-credentials $GITLAB_CD_SERVICEACCOUNT_NAME --token $GITLAB_CD_SERVICEACCOUNT_TOKEN
kubectl config set-context default --cluster=$KUBE_NAME --user=$GITLAB_CD_SERVICEACCOUNT_NAME
kubectl config use-context default